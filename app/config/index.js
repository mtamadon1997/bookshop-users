'use strict'

//Load Config Files
const dotenv = require('dotenv')
dotenv.config()

//Set env
const env = process.env.NODE_ENV || 'development'
//Main configs
const configs = {
  base: {
    env,
    name: process.env.APP_NAME || 'My API',
    host: process.env.APP_HOST || '0.0.0.0',
    port: 7071
  },
  production: {
    port: process.env.APP_PORT || 7071
  },
  development: {
  },
  test: {
    port: 7072,
  }
}
const config = Object.assign(configs.base, configs[env]) 

module.exports = config 
