'use strict'

//Load Config Files
const dotenv = require('dotenv')
dotenv.config()

//Set env
const env = process.env.NODE_ENV || 'development'
//Kong configs
module.exports = {
  env,
  server: process.env.KONG_SERVER || '127.0.0.1:8001',
  token: process.env.KONG_TOKEN || '',
  status: (process.env.KONG_STATUS == 'false') ? false:true
}
