const knexConfig = require('../../knexfile')
const dotenv = require('dotenv')
dotenv.config()
const Database = require('knex')(knexConfig[process.env.NODE_ENV || 'development'])
const { validate } = require('indicative').validator
class BaseModel {
  constructor(data) {
    for (const item in data) {
      this[item] = data[item]
    }
  }
  static get tableName() {
    return 'bases'
  }
  static get fields() {
    return {
      id: 'number'
    }
  }
  static get filters() {
    return ['id']
  }
  static get hidden() {
    return []
  }
  static async beforeCreate(data) {
    return data
  }
  static async beforeUpdate(data) {
    return data
  }
  static async afterCreate(data) {

  }
  static async afterUpdate(data) {

  }
  static async validate(data) {
    try {
      await validate(data, this.fields)
    } catch (error) {
      return {
        status: false,
        error: error
      }
    }
    return {
      status: true,
      error: null
    }
  }
  static async create(data) {
    data = await this.beforeCreate(data)
    let [id] = await this.query().insert(data)
    await this.afterCreate(data)
    for (const key in data) {
      if (this.fields[key].includes('array')) {
        data[key] = JSON.parse(data[key])
      }
    }
    return {
      id,
      ...data
    }
  }
  static async update(query, data) {
    data = await this.beforeUpdate(data)
    await this.query().where(query).update(data)
    await this.afterUpdate(data)
  }
  static async findBy(field, value) {
    const row = await this.query().where(field, value).first()
    if(!row){
      return null
    }
  
    for (const item of this.hidden) {
      delete row[item]
    }
    for (const field in row) {
      if (!this.fields[field] && field != 'id') {
        delete row[field]
      } else if (field != 'id' && this.fields[field].includes('array')) {
        row[field] = JSON.parse(row[field])
      }
    }
    return row

  }
  static async list(query = {}, sortBy = 'id', sortType = 'asc') {
    //only sort by fields
    if (!this.fields[sortBy]) {
      sortBy = 'id'
    }
    let list = this.query()
    for (const item in query) {
      if (this.filters.includes(item)) {
        if (this.fields[item].includes('string') || this.fields[item].includes('array')) {
          list = list.where(item, 'LIKE', '%' + query[item] + '%')
        } else {
          list = list.where(item, query[item])
        }
      }
    }
    list = await list.orderBy(sortBy, sortType).select('*')
    for (const key in list) {
      for (const item of this.hidden) {
        delete list[key][item]
      }
      for (const field in list[key]) {
        if (!this.fields[field] && field != 'id') {
          delete list[key][field]
        } else if (field != 'id' && this.fields[field].includes('array')) {
          list[key][field] = JSON.parse(list[key][field])
        }
      }
    }
    return list
  }
  static async deleteBy(field, value) {
    return await this.query().where(field, value).delete()
  }
  static query() {
    return Database(this.tableName)
  }

}
module.exports = BaseModel