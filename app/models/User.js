'use strict'

const kong = require('../utils/kong')
const BaseModel = require('./BaseModel')
const sha256 = require('sha256')
class User extends BaseModel{
  static get fields(){
    return {
      username: 'required|string|min:3',
      password: 'required|string|min:6',
      name: 'required|string|min:3'
    }
  }
  static get hidden(){
    return ['password']
  }
  static get tableName(){
    return 'users'
  }
  static async beforeCreate(data){
    data.password = sha256(process.env.APP_KEY + data.password)
    return data
  }
  static async afterCreate(data){
    //add user to kong
    await kong.storeConsumer(data.username)
  }
  static async checkCredential({username, password}){
    if(!username || !password){
      return {
        status: false,
        error:{
          message: 'Username & Password in required'
        },
        user: null
      }
    }
    password = sha256(process.env.APP_KEY + password)
    const user = await this.query().where({username: username, password: password}).first()
    if(!user){
      return {
        status: false,
        error:{
          message: 'Username or Password is incorrect'
        },
        user: null
      }
    }
    delete user.password
    return {
      status: true,
      error: null,
      user: user
    }
  }
}
module.exports = User