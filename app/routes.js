'use strict' 

const Router = require('koa-router') 
const AuthController = require('./controllers/AuthController')
const router = new Router()
router.get('/',(ctx)=>{
  ctx.response.body = {
    dasd:"ddd"
  }
})
router.post('/api/v1/auth/signup', AuthController.signup)
router.post('/api/v1/auth/login', AuthController.login)

module.exports = router 
