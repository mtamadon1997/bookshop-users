'use strict' 

const Koa = require('koa') 
const bodyParser = require('./middlewares/body-parser') 
const cors = require('./middlewares/cors') 
const corsConfig = require('./config/cors') 
const router = require('./routes') 


class App extends Koa {
  constructor(...params) {
    super(...params) 

    // Trust proxy
    this.proxy = true 
    // Disable `console.errors` except development env
    this.silent = this.env !== 'development' 

    this.servers = [] 

    this._configureMiddlewares() 
    this._configureRoutes() 
  }

  _configureMiddlewares() {
    this.use(
      bodyParser({
        enableTypes: ['json'],
        jsonLimit: '1mb'
      })
    ) 
    this.use(
      cors({
        origins: corsConfig.origins,
        allowMethods: ['GET', 'HEAD', 'PUT', 'POST', 'DELETE', 'PATCH'],
        allowHeaders: ['Content-Type', 'Authorization'],
        exposeHeaders: ['Content-Length', 'Date']
      })
    ) 
  }

  _configureRoutes() {
    // Bootstrap application router
    this.use(router.routes()) 
    this.use(router.allowedMethods()) 
  }

  listen(...args) {
    const server = super.listen(...args) 
    this.servers.push(server) 
    return server 
  }

  async terminate() {
    for (const server of this.servers) {
      server.close() 
    }
  }
}

module.exports = App 
