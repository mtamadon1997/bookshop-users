'user strict'
const User = require('../models/User')
const { generateToken } = require('../utils/kong')
class AuthController {
  /**
   * Register user and generate token
   * @param {*} ctx 
   */
  static async signup(ctx) {
    const check = await User.validate(ctx.request.body)
    if (!check.status) {
      ctx.status = 400
      ctx.body = {
        error: check.error[0]
      }
      return
    }
    const checkExist = await User.findBy('username', ctx.request.body.username)
    if (checkExist) {
      ctx.status = 400
      ctx.body = {
        error: {
          message: 'User already Exists'
        }
      }
      return
    }
    let user
    try {
      user = await User.create(ctx.request.body)
    } catch (error) {
      console.error(error)
      ctx.status = 500
      ctx.body = {
        error: {
          message: 'Unexpected Server Error'
        }
      }
      return
    }
    delete user.password
    const token = await generateToken(user.username)
    ctx.status = 201
    ctx.body = {
      token,
      user
    }
  }
  /**
   * Login and generate token for user
   * @param {*} ctx 
   */
  static async login(ctx) {
    const check = await User.checkCredential(ctx.request.body)
    if (!check.status) {
      ctx.status = 400
      ctx.body = {
        error: check.error
      }
      return
    }
    //generate token (using kong)
    const token = await generateToken(check.user.username)
    ctx.body = {
      token,
      user: check.user
    }
  }
}
module.exports = AuthController