const config = require('../config/kong')
const axios = require('axios')
module.exports = {
  storeConsumer : async (username) =>{
    // ignore if kong is disabled
    if(!config.status){
      return true
    }
    //check if exists
    try {
      await axios({
        method: 'GET',
        url: `http://${config.server}/consumers/${username}`,
        data: {
          username
        },
        headers : {
          authorization: config.token
        }
      })
      return true
    } catch (error) {
      //do nothing and go to next step
    }
    //try to add new consumer
    try {
      await axios({
        method: 'POST',
        url: `http://${config.server}/consumers`,
        data: {
          username
        },
        headers : {
          authorization: config.token
        }
      })
      return true
    } catch (error) {
      console.log(error)
      return false
    }
  },
  generateToken : async (username) =>{
    // send 'disabled' as token if kong disabled
    if(!config.status){
      return 'null'
    }
    try {
      let res = await axios({
        method: 'POST',
        url: `http://${config.server}/consumers/${username}/key-auth`,
        headers : {
          authorization: config.token
        }
      })
      return res.data.key
    } catch (error) {
      console.log(error)
      return 'null'
    }
  }
}