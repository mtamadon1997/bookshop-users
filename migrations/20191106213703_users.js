
exports.up = async function(knex) {
  await knex.schema.createTable('users', function (table) {
    table.increments('id')
    table.string('username').unique().notNullable()
    table.string('name').notNullable()
    table.string('password').notNullable()
  })
};

exports.down = function(knex) {
  knex.schema.dropTable('users')
};
